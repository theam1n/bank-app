package com.example.bankapp.dto;

import com.example.bankapp.entity.Account;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransactionDto {

    private Long id;

    private AccountDto fromAccount;

    private AccountDto toAccount;

    private double amount;
    private LocalDate date;
    private String type;
}
