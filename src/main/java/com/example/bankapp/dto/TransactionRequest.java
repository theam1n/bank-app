package com.example.bankapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransactionRequest {

    private AccountDto fromAccount;

    private AccountDto toAccount;

    private double amount;
    private LocalDate date;
    private String type;
}
