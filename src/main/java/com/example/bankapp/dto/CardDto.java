package com.example.bankapp.dto;

import com.example.bankapp.entity.Account;
import com.example.bankapp.entity.CardBenefit;
import jakarta.persistence.CascadeType;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CardDto {

    private Long id;

    private AccountDto account;

    private String cardNumber;
    private String cardName;
    private String expirationDate;

    private List<CardBenefitDto> benefits;
}
