package com.example.bankapp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CardRequest {

    private AccountDto account;

    private String cardNumber;
    private String cardName;
    private String expirationDate;

    private List<CardBenefitRequest> benefits;
}
