package com.example.bankapp.service.impl;

import com.example.bankapp.dto.UserDto;
import com.example.bankapp.dto.UserRequest;
import com.example.bankapp.entity.Address;
import com.example.bankapp.entity.User;
import com.example.bankapp.exception.NotFoundException;
import com.example.bankapp.mapper.UserMapper;
import com.example.bankapp.repository.AddressRepository;
import com.example.bankapp.repository.UserRepository;
import com.example.bankapp.service.UserService;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final AddressRepository addressRepository;

    @Override
    public UserDto saveUser(UserRequest userRequest) {

        var user = UserMapper.INSTANCE.requestToEntity(userRequest);
        var userdb = userRepository.save(user);

        var address = user.getAddress();
        address.setUser(userdb);
        addressRepository.save(address);

        var response = UserMapper.INSTANCE.entityToDto(userdb);

        return response;
    }

    @Override
    public UserDto getUser(Long id) {

        Optional<User> optionalUser = Optional.ofNullable(userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("User not found with id: " + id)));

        var user = optionalUser.get();
        var response = UserMapper.INSTANCE.entityToDto(user);


        return response;
    }

    @Override
    public List<UserDto> getAllUsers() {

        var users = userRepository.findAll();
        var response = users.stream()
                .map(UserMapper.INSTANCE:: entityToDto)
                .collect(Collectors.toList());

        return response;
    }

    @Override
    public UserDto editUser(UserDto user) {

        Optional<User> optionalUser = Optional.ofNullable(userRepository.findById(user.getId())
                .orElseThrow(() -> new NotFoundException("User not found with id: " + user.getId())));

        var existingUser = optionalUser.get();

        Optional.ofNullable(user.getUsername()).ifPresent(existingUser::setUsername);

        var response = UserMapper.INSTANCE
                .entityToDto(userRepository.save(existingUser));

        return response;
    }

    @Override
    public void deleteUser(Long id) {

        var user = userRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("User not found with id : " + id));

        userRepository.deleteById(id);

    }
}
