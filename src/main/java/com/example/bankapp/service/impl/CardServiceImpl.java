package com.example.bankapp.service.impl;

import com.example.bankapp.dto.CardDto;
import com.example.bankapp.dto.CardRequest;
import com.example.bankapp.entity.Card;
import com.example.bankapp.exception.NotFoundException;
import com.example.bankapp.mapper.CardMapper;
import com.example.bankapp.repository.CardRepository;
import com.example.bankapp.service.CardService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CardServiceImpl implements CardService {

    private final CardRepository cardRepository;


    @Override
    public CardDto saveCard(CardRequest cardRequest) {

        var card = CardMapper.INSTANCE.requestToEntity(cardRequest);
        var response = CardMapper.INSTANCE.entityToDto(cardRepository.save(card));

        return response;
    }

    @Override
    public CardDto getCard(Long id) {

        Optional<Card> optionalCard = Optional.ofNullable(cardRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Card not found with id: " + id)));

        var card = optionalCard.get();
        var response = CardMapper.INSTANCE.entityToDto(card);


        return response;
    }

    @Override
    public List<CardDto> getAllCards() {

        var cards = cardRepository.findAll();
        var response = cards.stream()
                .map(CardMapper.INSTANCE:: entityToDto)
                .collect(Collectors.toList());

        return response;
    }

    @Override
    public CardDto editCard(CardDto card) {

        Optional<Card> optionalCard = Optional.ofNullable(cardRepository.findById(card.getId())
                .orElseThrow(() -> new NotFoundException("Card not found with id: " + card.getId())));

        var existingCard = optionalCard.get();

        Optional.ofNullable(card.getCardName()).ifPresent(existingCard::setCardName);
        Optional.ofNullable(card.getCardNumber()).ifPresent(existingCard::setCardNumber);
        Optional.ofNullable(card.getExpirationDate()).ifPresent(existingCard::setExpirationDate);

        var response = CardMapper.INSTANCE
                .entityToDto(cardRepository.save(existingCard));

        return response;
    }

    @Override
    public void deleteCard(Long id) {

        var card = cardRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Card not found with id : " + id));

        cardRepository.deleteById(id);

    }

}
