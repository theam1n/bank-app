package com.example.bankapp.service.impl;

import com.example.bankapp.dto.AccountDto;
import com.example.bankapp.dto.AccountRequest;
import com.example.bankapp.entity.Account;
import com.example.bankapp.exception.NotFoundException;
import com.example.bankapp.mapper.AccountMapper;
import com.example.bankapp.repository.AccountRepository;
import com.example.bankapp.service.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;


    @Override
    public AccountDto saveAccount(AccountRequest accountRequest) {

        var account = AccountMapper.INSTANCE.requestToEntity(accountRequest);
        var response = AccountMapper.INSTANCE.entityToDto(accountRepository.save(account));

        return response;
    }

    @Override
    public AccountDto getAccount(Long id) {

        Optional<Account> optionalAccount = Optional.ofNullable(accountRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Account not found with id: " + id)));

        var account = optionalAccount.get();
        var response = AccountMapper.INSTANCE.entityToDto(account);


        return response;
    }

    @Override
    public List<AccountDto> getAllAccounts() {

        var accounts = accountRepository.findAll();
        var response = accounts.stream()
                .map(AccountMapper.INSTANCE:: entityToDto)
                .collect(Collectors.toList());

        return response;
    }

    @Override
    public AccountDto editAccount(AccountDto account) {

        Optional<Account> optionalAccount = Optional.ofNullable(accountRepository.findById(account.getId())
                .orElseThrow(() -> new NotFoundException("Account not found with id: " + account.getId())));

        var existingAccount = optionalAccount.get();

        Optional.ofNullable(account.getAccountNumber()).ifPresent(existingAccount::setAccountNumber);
        Optional.ofNullable(account.getBalance()).ifPresent(existingAccount::setBalance);

        var response = AccountMapper.INSTANCE
                .entityToDto(accountRepository.save(existingAccount));

        return response;
    }

    @Override
    public void deleteAccount(Long id) {

        var account = accountRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Account not found with id : " + id));

        accountRepository.deleteById(id);

    }

}
