package com.example.bankapp.service.impl;

import com.example.bankapp.dto.TransactionDto;
import com.example.bankapp.dto.TransactionRequest;
import com.example.bankapp.entity.Transaction;
import com.example.bankapp.exception.NotFoundException;
import com.example.bankapp.mapper.TransactionMapper;
import com.example.bankapp.repository.TransactionRepository;
import com.example.bankapp.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;

    @Override
    public TransactionDto saveTransaction(TransactionRequest transactionRequest) {

        var transaction = TransactionMapper.INSTANCE.requestToEntity(transactionRequest);
        var response = TransactionMapper.INSTANCE.entityToDto(transactionRepository.save(transaction));

        return response;
    }

    @Override
    public TransactionDto getTransaction(Long id) {

        Optional<Transaction> optionalTransaction = Optional.ofNullable(transactionRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Transaction not found with id: " + id)));

        var transaction = optionalTransaction.get();
        var response = TransactionMapper.INSTANCE.entityToDto(transaction);


        return response;
    }

    @Override
    public List<TransactionDto> getAllTransactions() {

        var transactions = transactionRepository.findAll();
        var response = transactions.stream()
                .map(TransactionMapper.INSTANCE:: entityToDto)
                .collect(Collectors.toList());

        return response;
    }

    @Override
    public TransactionDto editTransaction(TransactionDto transaction) {

        Optional<Transaction> optionalTransaction = Optional.ofNullable(transactionRepository.findById(transaction.getId())
                .orElseThrow(() -> new NotFoundException("Transaction not found with id: " + transaction.getId())));

        var existingTransaction = optionalTransaction.get();

        Optional.ofNullable(transaction.getAmount()).ifPresent(existingTransaction::setAmount);
        Optional.ofNullable(transaction.getDate()).ifPresent(existingTransaction::setDate);
        Optional.ofNullable(transaction.getType()).ifPresent(existingTransaction::setType);

        var response = TransactionMapper.INSTANCE
                .entityToDto(transactionRepository.save(existingTransaction));

        return response;
    }

    @Override
    public void deleteTransaction(Long id) {

        var transaction = transactionRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Transaction not found with id : " + id));

        transactionRepository.deleteById(id);

    }
}
