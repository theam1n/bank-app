package com.example.bankapp.service;

import com.example.bankapp.dto.AccountDto;
import com.example.bankapp.dto.AccountRequest;

import java.util.List;

public interface AccountService {

    AccountDto saveAccount(AccountRequest accountRequest);

    AccountDto getAccount(Long id);

    List<AccountDto> getAllAccounts();

    AccountDto editAccount(AccountDto account);

    void deleteAccount(Long id);
}
