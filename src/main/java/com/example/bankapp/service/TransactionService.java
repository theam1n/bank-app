package com.example.bankapp.service;

import com.example.bankapp.dto.TransactionDto;
import com.example.bankapp.dto.TransactionRequest;

import java.util.List;

public interface TransactionService {

    TransactionDto saveTransaction(TransactionRequest transactionRequest);

    TransactionDto getTransaction(Long id);

    List<TransactionDto> getAllTransactions();

    TransactionDto editTransaction(TransactionDto transaction);

    void deleteTransaction(Long id);
}
