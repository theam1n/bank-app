package com.example.bankapp.service;

import com.example.bankapp.dto.UserDto;
import com.example.bankapp.dto.UserRequest;

import java.util.List;

public interface UserService {

    UserDto saveUser(UserRequest userRequest);

    UserDto getUser(Long id);

    List<UserDto> getAllUsers();

    UserDto editUser(UserDto user);

    void deleteUser(Long id);
}
