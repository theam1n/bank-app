package com.example.bankapp.service;

import com.example.bankapp.dto.CardDto;
import com.example.bankapp.dto.CardRequest;

import java.util.List;

public interface CardService {

    CardDto saveCard(CardRequest cardRequest);

    CardDto getCard(Long id);

    List<CardDto> getAllCards();

    CardDto editCard(CardDto card);

    void deleteCard(Long id);
}
