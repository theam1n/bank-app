package com.example.bankapp.exception;

import com.example.bankapp.dto.ExceptionDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ExceptionDto> dataNotFound(NotFoundException ex) {

        int status = HttpStatus.NOT_FOUND.value();
        return ResponseEntity.status(status)
                .body(ExceptionDto.builder()
                        .status(status)
                        .message(ex.getMessage())
                        .build());
    }
}
