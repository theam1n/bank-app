package com.example.bankapp.controller;

import com.example.bankapp.dto.UserDto;
import com.example.bankapp.dto.UserRequest;
import com.example.bankapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/user")
public class UserController {

    private final UserService userService;

    @PostMapping
    public ResponseEntity<UserDto> saveUser(
            @RequestBody UserRequest userRequest) {

        UserDto response = userService.saveUser(userRequest);

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUser(
            @PathVariable Long id) {

        UserDto response = userService.getUser(id);

        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<List<UserDto>> getAllUsers(){

        List<UserDto> response = userService.getAllUsers();

        return ResponseEntity.ok(response);
    }

    @PutMapping
    public ResponseEntity<UserDto> editUser(
            @RequestBody UserDto user) {

        UserDto response = userService.editUser(user);

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public void deleteUser(@PathVariable Long id){

        userService.deleteUser(id);

    }

}
