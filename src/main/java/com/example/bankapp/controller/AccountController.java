package com.example.bankapp.controller;

import com.example.bankapp.dto.AccountDto;
import com.example.bankapp.dto.AccountRequest;
import com.example.bankapp.service.impl.AccountServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/account")
public class AccountController {

    private final AccountServiceImpl accountService;

    @PostMapping
    public ResponseEntity<AccountDto> saveAccount(
            @RequestBody AccountRequest accountRequest) {

        AccountDto response = accountService.saveAccount(accountRequest);

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<AccountDto> getAccount(
            @PathVariable Long id) {

        AccountDto response = accountService.getAccount(id);

        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<List<AccountDto>> getAllAccounts(){

        List<AccountDto> response = accountService.getAllAccounts();

        return ResponseEntity.ok(response);
    }

    @PutMapping
    public ResponseEntity<AccountDto> editAccount(
            @RequestBody AccountDto account) {

        AccountDto response = accountService.editAccount(account);

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public void deleteAccount(@PathVariable Long id){

        accountService.deleteAccount(id);

    }

}
