package com.example.bankapp.controller;

import com.example.bankapp.dto.CardDto;
import com.example.bankapp.dto.CardRequest;
import com.example.bankapp.service.impl.CardServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/card")
public class CardController {

    private final CardServiceImpl cardService;

    @PostMapping
    public ResponseEntity<CardDto> saveCard(
            @RequestBody CardRequest cardRequest) {

        CardDto response = cardService.saveCard(cardRequest);

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CardDto> getCard(
            @PathVariable Long id) {

        CardDto response = cardService.getCard(id);

        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<List<CardDto>> getAllCards(){

        List<CardDto> response = cardService.getAllCards();

        return ResponseEntity.ok(response);
    }

    @PutMapping
    public ResponseEntity<CardDto> editCard(
            @RequestBody CardDto card) {

        CardDto response = cardService.editCard(card);

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public void deleteCard(@PathVariable Long id){

        cardService.deleteCard(id);

    }
}
