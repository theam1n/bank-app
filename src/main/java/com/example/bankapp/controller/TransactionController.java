package com.example.bankapp.controller;

import com.example.bankapp.dto.TransactionDto;
import com.example.bankapp.dto.TransactionRequest;
import com.example.bankapp.service.impl.TransactionServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/transaction")
public class TransactionController {

    private final TransactionServiceImpl transactionService;

    @PostMapping
    public ResponseEntity<TransactionDto> saveTransaction(
            @RequestBody TransactionRequest transactionRequest) {

        TransactionDto response = transactionService.saveTransaction(transactionRequest);

        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<TransactionDto> getTransaction(
            @PathVariable Long id) {

        TransactionDto response = transactionService.getTransaction(id);

        return ResponseEntity.ok(response);
    }

    @GetMapping
    public ResponseEntity<List<TransactionDto>> getAllTransactions(){

        List<TransactionDto> response = transactionService.getAllTransactions();

        return ResponseEntity.ok(response);
    }

    @PutMapping
    public ResponseEntity<TransactionDto> editTransaction(
            @RequestBody TransactionDto transaction) {

        TransactionDto response = transactionService.editTransaction(transaction);

        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/{id}")
    public void deleteTransaction(@PathVariable Long id){

        transactionService.deleteTransaction(id);

    }

}
