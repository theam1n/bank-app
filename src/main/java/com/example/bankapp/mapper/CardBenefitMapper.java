package com.example.bankapp.mapper;

import com.example.bankapp.dto.CardBenefitDto;
import com.example.bankapp.dto.CardBenefitRequest;
import com.example.bankapp.entity.CardBenefit;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CardBenefitMapper {

    public static final CardBenefitMapper INSTANCE = Mappers.getMapper(CardBenefitMapper.class);

    @Mapping(target = "id", ignore = true)
    CardBenefit requestToEntity(CardBenefitRequest cardBenefitRequest);

    CardBenefitDto entityToDto(CardBenefit cardBenefit);
}
