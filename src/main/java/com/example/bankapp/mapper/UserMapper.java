package com.example.bankapp.mapper;

import com.example.bankapp.dto.UserDto;
import com.example.bankapp.dto.UserRequest;
import com.example.bankapp.entity.User;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface UserMapper {

    public static final UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "id", ignore = true)
    User requestToEntity(UserRequest userRequest);

    UserDto entityToDto(User user);
}
