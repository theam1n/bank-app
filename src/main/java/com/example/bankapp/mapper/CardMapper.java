package com.example.bankapp.mapper;

import com.example.bankapp.dto.CardDto;
import com.example.bankapp.dto.CardRequest;
import com.example.bankapp.entity.Card;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface CardMapper {

    public static final CardMapper INSTANCE = Mappers.getMapper(CardMapper.class);

    @Mapping(target = "id", ignore = true)
    Card requestToEntity(CardRequest cardRequest);

    CardDto entityToDto(Card card);
}
