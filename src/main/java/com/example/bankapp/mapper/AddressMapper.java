package com.example.bankapp.mapper;

import com.example.bankapp.dto.AddressDto;
import com.example.bankapp.dto.AddressRequest;
import com.example.bankapp.entity.Address;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface AddressMapper {

    public static final AddressMapper INSTANCE = Mappers.getMapper(AddressMapper.class);

    @Mapping(target = "id", ignore = true)
    Address requestToEntity(AddressRequest addressRequest);

    AddressDto entityToDto(Address address);
}
